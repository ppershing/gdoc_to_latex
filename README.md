# Google Documents to Latex converter

This is a quickly hacked script which can fetch a Google document and convert it to a reasonable-looking latex document. It is however not perfect (nor it is intended to)

## Basic usage
Your Google document needs to be published (File/publish).
After publishing, you can get document id from published URL such as https://docs.google.com/document/d/1jB2dRQTN_1vU0tEAfzbjgr3hjRZK5CO1531V5zgTt8k/pub by simply by taking the relevant document fragment and just run
 
`./gdoc_to_latex.py --docid=1jB2dRQTN_1vU0tEAfzbjgr3hjRZK5CO1531V5zgTt8k`

`pdflatex main.tex`

## Features

Gdoc-to-latex can handle both _italics_ and **bold**. It can also handle tables and figures, footnotes and equations. The produced latex file is not perfect but it can serve as a quick starting point.

## Get it
Gdoc-to-latex was developed by Peter Peresini as a quick way to check how long the academic paper will be in latex but still writing it mainly in Google documents. You can get Gdoc-to-latex from the official repository at https://bitbucket.org/ppershing/gdoc_to_latex

## Todos
  - generally better indentation of the output
  - support \texttt for Courier font

## Changelog

 - 2014-05-04: Version 0.1
   - Support tables, figures, bold, italics, footnotes, equations, numbered and unnumbered lists
 - 2014-04-30: Initial version 0.0 
