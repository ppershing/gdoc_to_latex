#!/usr/bin/python
# -*- coding: utf-8 -*-
import bs4
import re
import urllib2
import sys
import argparse
import urlparse
import cssutils
import logging

logging.basicConfig()
log = logging.getLogger('gdoc_to_latex')
log.setLevel(logging.INFO)

parser = argparse.ArgumentParser(description="Download google doc and convert it to latex file")
source_group = parser.add_mutually_exclusive_group(required=True)
source_group.add_argument("--docid")
source_group.add_argument("--file")
parser.add_argument("--out", default="content.tex")
parser.add_argument("--download-figures", default=False, action="store_true")

FLAGS = parser.parse_args()

def wrap_children_in(to_wrap, wrapper):
    for c in list(to_wrap.children):
        wrapper.append(c)
    to_wrap.append(wrapper)

def retrieve_document():
    log.info("Retrieving document...")

    if FLAGS.file:
        f_in = open(FLAGS.file)
    elif FLAGS.docid:
        url="https://docs.google.com/document/d/%s/pub" % FLAGS.docid
        f_in = urllib2.urlopen(url)
    else:
        assert False
    soup = bs4.BeautifulSoup(f_in, from_encoding='utf8')
    return soup


def parse_styles(soup):
    log.info("Parsing stylesheets")

    bold_classes = set()
    emph_classes = set()
    tt_classes = set()
    # Parse stylesheets
    for s in soup.findAll("style"):
        for rule in cssutils.parseString(s.string):
            try:
                rule.selectorList
            except:
                continue
            if (len(rule.selectorList) != 1):
                continue
            selector = rule.selectorList[0].selectorText
            if not selector[0]==".":
                continue # not a class
            for prop in rule.style.getProperties():
                if prop.name == "font-style" and prop.value=="italic":
                    emph_classes.add(selector[1:])
                if prop.name == "font-weight" and prop.value == "bold":
                    bold_classes.add(selector[1:])
                if prop.name == "font-family" and prop.value == '"Courier New"':
                    tt_classes.add(selector[1:])

    log.info("CSS bold classes: %s", bold_classes)
    log.info("CSS emph classes: %s", emph_classes)
    log.info("CSS   tt classes: %s", tt_classes)
    return {"textbf": bold_classes, "emph": emph_classes, "texttt": tt_classes}


def transform_preclean(soup):
    """Get rid of unnecessary parts of the document"""
    # Get rid of doctype
    for s in soup.contents:
        if isinstance(s, bs4.Doctype):
            s.extract()

    # Get rid of Google Docs footer
    for s in soup.findAll(id="header"):
        s.extract()
    for s in soup.findAll(id="footer"):
        s.extract()

    # Ok, we don"t need these
    for s in soup.findAll(["head", "script", "style", "hr"]):
        s.extract()
    return


def transform_move_footnotes(soup):
    """Footnotes are referenced in text but content is at the end of the document.
    We need to move them around.
    """
    # Example footnote:
    # <span>text before footnote</span><sup><a href="#ftnt1" name="ftnt_ref1">[1]</a></sup>
    #
    # end of document <a href="#ftnt_ref1" name="ftnt1">[1]</a><span class="c16"> Footnote </span>
    for s in soup.findAll("sup"):
        footnote_anchor = soup.find(attrs={"name": s.a['href'][1:]})
        s.a.extract()
        f = soup.new_tag("latex_footnote")
        f.append(footnote_anchor.parent) # move footnote content
        footnote_anchor.extract() # anchor not needed
        s.append(f)
        s.unwrap()
    return


def transform_parse_equations(soup):
    def simplify_equation(text):
        """Gdocs have an uncanny habit of putting parenthesis around single-letter variables.
        For readability we will therefore undo {X}_{i} into X_i
        """
        # Note: we should not overdo it as \sqrt{A} should not be replaced with sqrtA
        # replace _{c} with _c
        text =  re.compile(r'_{(.)}').sub(r'_\1', text)
        # replace {c}_ with c_. Warning: need to be careful not to have something before, e.g. \sqrt{c}
        text =  re.compile(r'([^a-zA-Z0-9\\]|^){(.)}_').sub(r'\1\2_', text)
        # replace {} with space
        text = text.replace("{}", " ")
        return text

    for img in soup.findAll("img"):
        if img['src'].startswith("https://www.google.com/chart"):
            # This is equation!!!
            query = urlparse.parse_qs(urlparse.urlparse(img['src']).query)
            eq = soup.new_tag("latex_equation")
            eq.append(simplify_equation(query['chl'][0]))
            img.replace_with(eq)
    return

def insert_bold_italics(soup, s):
    classes = set(unicode(x) for x in s.get("class", []))
    if "subtitle" in classes:
        return;
    if classes & styles["texttt"]:
        wrap_children_in(s, soup.new_tag("latex_tt"))
    if classes & styles["textbf"]:
        wrap_children_in(s, soup.new_tag("latex_bf"))
    if classes & styles["emph"]:
        wrap_children_in(s, soup.new_tag("latex_emph"))

def transform_bold_italics(soup):
    # Here only the content inside is interesting
    for s in soup.findAll(["span", "p"]):
        insert_bold_italics(soup, s)


def transform_unwrap(soup):
    """Get rid of tags which do not represent any useful tree info"""
    for s in soup.findAll(["html", "body", "span", "div", "sub"]):
        s.unwrap()

def transform_headings(soup):
    """Transform headings"""
    HTML_TO_LATEX_HEADERS = {
        "h1": "latex_section",
        "h2": "latex_subsection",
        "h3": "latex_subsubsection",
    }

    for tag_name, pattern in HTML_TO_LATEX_HEADERS.items():
        for s in soup.findAll(tag_name):
            try:
                s.a.unwrap() # headings have redundant <a> link
            except:
                log.error("Nonstandard header (not having <a href=...>) %s" %s)
            wrap_children_in(s, soup.new_tag(pattern))
            s.unwrap()

def get_list_indent_level(s):
    for c in s.get("class", []):
        if c.startswith("lst"):
            return int(c.split("-")[-1])
    log.error("Something is broken, no indent found for list item %s" % s)
    return 0


def transform_fix_nested_lists(s):
    n = s.next_sibling
    if not n:
        return

    if isinstance(n, bs4.Tag) and n.name in ["ul", "ol"]:
        transform_fix_nested_lists(n) # first recursively fix it
        nn = n.next_sibling
        if not nn or not isinstance(nn, bs4.Tag) or n.name not in ["ul", "ol"]:
            return
        i1 = get_list_indent_level(s)
        i2 = get_list_indent_level(n)
        i3 = get_list_indent_level(nn)
        # seems suspiciously like a nested list
        if i1 == i3 and i1 + 1 == i2:
            s.append(n)
            s.append(nn)
            nn.unwrap()


def transform_lists(soup):
    for s in soup.findAll("ul"):
        s.wrap(soup.new_tag("latex_itemize"))
        s.unwrap()
    for s in soup.findAll("ol"):
        s.wrap(soup.new_tag("latex_enumerate"))
        s.unwrap()
    for s in soup.findAll("li"):
        s.wrap(soup.new_tag("latex_item"))
        s.unwrap()

def transform_br(soup):
    for s in soup.findAll("br"):
        s.append(r"\newline ")
        s.unwrap()

def get_caption(s, prefix):
    for x in s.findAllNext(True):
        if x.name == "table":
            log.warn("No caption found till the next table")
            return "Missing caption"
        if x.name == "img":
            log.warn("No caption found till the next image")
            return "Missing caption"
        if "subtitle" in x.get("class", []):
            text = "".join(x.findAll(text=True))
            log.info("Found caption: %s", text)
            if text.startswith(prefix):
                # and we should strip the prefix, hopefully it is only inside a single text element
                y = x.find(text=True)
                if y.string.startswith(prefix):
                    y.replace_with(y.string[len(prefix):])
                return x
            else:
                log.warning("Caption invalid, does not start with %s", prefix)
                return "Missing caption"
    log.warning("No caption found till the end of the document")
    return "Missing caption"

def transform_tables(soup):
    for s in soup.findAll("table"):
        maxcol = 0
        for i, row in enumerate(s.findAll("tr")):
            row.wrap(soup.new_tag("latex_array_row"))
            for j, col in enumerate(row.findAll("td")):
                if j == 0:
                    typ = "latex_array_cell_first"
                else:
                    typ = "latex_array_cell"
                col.wrap(soup.new_tag(typ))
                col.unwrap()
                maxcol = max(maxcol, j)
            row.unwrap()
        
        table = soup.new_tag("latex_table")
        s.wrap(table)
        tabular = soup.new_tag("latex_tabular")
        tabular["align"] = "|".join([""] + ["c" for i in range(1 + maxcol)] + [""])
        s.tbody.wrap(tabular)
        s.tbody.unwrap()
        caption = get_caption(s, "Table:")
        caption_node = soup.new_tag("latex_caption")
        caption_node.append(caption)
        table.append(caption_node)
        s.unwrap()

def transform_figures(soup):
    drawing_no = 1
    for s in soup.findAll("img"):
        assert not s['src'].startswith("https://www.google.com/chart")
        # This is figure or drawing
        if s['alt']:
            filename = s['alt']
        else:
            filename = "drawing_%d.png" % drawing_no
            drawing_no += 1

        path = "figures/%s" % filename
        figure = soup.new_tag("latex_figure")
        s.wrap(figure)
        img = soup.new_tag("latex_graphics")
        img.append(path)
        figure.append(img)
        caption = get_caption(s, "Figure:")
        caption_node = soup.new_tag("latex_caption")
        caption_node.append(caption)
        figure.append(caption_node)
        s.unwrap()
        
        if FLAGS.download_figures:
            log.info("Downloading figure %s", path)
            with open(path, "w") as f:
                f.write(urllib2.urlopen(s['src']).read())


def transform_remove_paragraphs(soup):
    STRIP_PARAGRAPHS_FROM = [
        "latex_tabular",
        "latex_section",
        "latex_subsection",
        "latex_subsubsection",
        "latex_footnote",
    ]
    for s in soup.findAll(STRIP_PARAGRAPHS_FROM):
        for p in s.findAll("p"):
            p.unwrap()

def transform_cleanup(soup):
    for a in soup.findAll("a"):
        if a.get("href", "") in ["", "#"]:
            a.unwrap()
    for a in soup.findAll(["latex_emph", "latex_bf"]):
        if not list(a.children):
            a.unwrap()
    ## Merge consecutive emph's or bf's
    changed = True
    while changed:
        changed = False
        for a in soup.findAll(["latex_emph", "latex_bf"]):
            n = a.next_sibling
            if not isinstance(n, bs4.Tag):
                continue
            if n.name == a.name: # same tag
                a.append(n)
                n.unwrap()
                changed = True
            elif n.name == "latex_equation":
                a.append(n)
                changed = True

soup = retrieve_document()
styles = parse_styles(soup)

transform_preclean(soup)
transform_br(soup)
transform_move_footnotes(soup)
transform_parse_equations(soup)
transform_bold_italics(soup)
transform_unwrap(soup)
transform_headings(soup)
for s in soup.findAll(["ul", "ol"]):
    transform_fix_nested_lists(s)
transform_lists(soup)
transform_tables(soup)
transform_figures(soup)
transform_remove_paragraphs(soup)
transform_cleanup(soup)
import collections
TagDescription = collections.namedtuple("TagDescription", ["pre", "post"])

class LatexRenderer(object):
    TAGS = {
        "latex_item" : TagDescription(["", "\\item "], ["", ""]),
        "latex_itemize" : TagDescription(["", "\\begin{{itemize}}", ""], ["", "\\end{{itemize}}", ""]),
        "latex_enumerate" : TagDescription(["", "\\begin{{enumerate}}", ""], ["", "\\end{{enumerate}}", ""]),
        "p" : TagDescription(["", ""], ["", ""]),
        "latex_section" : TagDescription(["", "", "\\section{{"], ["}}", ""]),
        "latex_subsection" : TagDescription(["", "", "\\subsection{{"], ["}}", ""]),
        "latex_subsubsection" : TagDescription(["", "", "\\subsubsection{{"], ["}}", ""]),
        "latex_emph" : TagDescription(["\\emph{{"], ["}}"]),
        "latex_tt" : TagDescription(["\\texttt{{\\detokenize{{"], ["}}}}"]),
        "latex_equation" : TagDescription(["$"], ["$"]),
        "latex_bf" : TagDescription(["\\textbf{{"], ["}}"]),
        "latex_footnote" : TagDescription(["\\footnote{{", ""], ["", "}}", ""]),
        
        "latex_table" : TagDescription(["", "\\begin{{table*}}", "  \\centering", ""], ["",
        "\\end{{table*}}", ""]),
        "latex_tabular" : TagDescription(["", "\\begin{{tabular}}{{{align}}}", "  "], ["\\hline", "\\end{{tabular}}", ""]),
        "latex_caption" : TagDescription(["", "\caption{{"], ["}}", ""]),
        "latex_array_cell_first" : TagDescription([""], [""]),
        "latex_array_cell" : TagDescription(["\t\t& "], [""]),
        "latex_array_row" : TagDescription(["\\hline  "], [" \\\\", ""]),
        
        "latex_figure" : TagDescription(["", "\\begin{{figure}}", "  \\centering", ""], ["", "\\end{{figure}}", ""]),
        "latex_graphics" : TagDescription(["", "\\includegraphics[width=0.4\\textwidth]{{"], ["}}", ""]),
        
        "a" : TagDescription(["\\href{{{href}}}{{\\detokenize{{"], ["}}}}"]),
        "[document]": TagDescription([""], [""]),
    }
    def __init__(self, soup):
        self.soup = soup

    def render(self):
        #return self._visit(soup)
        tmp = self._visit(soup)
        return "\n".join(s for s in tmp)

    def _visit(self, tag):
        if isinstance(tag, bs4.Tag):

            if tag.name in self.TAGS:
                info = self.TAGS[tag.name]
                pre = info.pre
                post = info.post
            else:
                pre = ["<"+tag.name+">", ""]
                post =["", "</" + tag.name+">"]

            res = [""]
            pre = [s.format(**tag.attrs) for s in pre]
            post = [s.format(**tag.attrs) for s in post]
            for c in tag.children:
                res2 = self._visit(c)
                res[-1] = res[-1] + res2[0]
                res = res + res2[1:]
            res[0] = pre[-1] + res[0]
            res[-1] = res[-1] + post[0]
            
            should_indent = tag.name != "[document]"
            if should_indent:
                res[1:] = ["  " + x for x in res[1:]] # inner indent
            return pre[:-1] + res + post[1:]
        else:
            # string
            text = tag.encode("utf-8")
            UTF_FIXES = [
                ("…", "\dots"),
                ("‘", "`"),
                ("’", "'"),
                ("“", "``"),
                ("”", "''"),
                # non-breakable space (special utf-8 character)
                (" ", " "),
                #(" ", "~"),
                # One sentence per line
                (". ", ".\n"),
                ("; ", ";\n "),
                (" -- ", " --\n  ")
            ]
            for search, repl in UTF_FIXES:
                text = text.replace(search, repl)
            return text.split("\n")

renderer = LatexRenderer(soup)
text=renderer.render()
#print soup.prettify().encode("utf-8")



# And just print out what we have
fout = open(FLAGS.out, "w")
print >>fout, text
